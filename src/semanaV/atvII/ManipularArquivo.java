package semanaV.atvII;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class ManipularArquivo {

	public <T> MyStack<T> lerArquivo(String caminhoArquivo) {
		StringBuilder conteudo = new StringBuilder();
		MyStack<Character> pilhaAmino = null;
		try {
			File arquivo = new File(caminhoArquivo);
			FileReader leitor = new FileReader(arquivo);
			int caractere;
			while ((caractere = leitor.read()) != -1) {
				conteudo.append((char) caractere);
			}
			pilhaAmino = new MyStack<>();

			for (char aminoAcid : conteudo.toString().toCharArray()) {
				pilhaAmino.push(aminoAcid);
			}
			leitor.close();
		} catch (IOException e) {
			System.out.println("Ocorreu um erro ao ler o arquivo: " + e.getMessage());
		}
		return (MyStack<T>) pilhaAmino;
	}

}
