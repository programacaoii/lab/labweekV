package semanaV.atvII;

public class MyStack<T> {
	private LSE topo;

	public boolean isEmpty() {
		if (this.topo == null) {
			return true;
		} else {
			return false;
		}
	}

	public void push(T value) { 
		LSE<T> novo = new LSE<>(value);

		if (this.isEmpty()) {
			topo = novo;
		} else {
			novo.setProx(this.topo);
			this.topo = novo;
		}
	}

	public T pop() throws ExceptionMenssage { 
		Object value;
		if (isEmpty()) {
			throw new ExceptionMenssage("A pilha está vazia.");
		} else {
			value = topo.getInfo();
			topo = topo.getProx();
		}
		return (T) value;
	}

	public T top() {
		return (T) this.topo.getInfo();
	}

	public boolean isFull() {
		return false;
	}
}
