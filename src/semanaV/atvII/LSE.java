package semanaV.atvII;

public class LSE<T> {
	private T info;
	private LSE prox;

	public LSE(T value) {
		this.info = value;
	}

	public void setInfo(T value) {
		this.info = value;
	}

	public T getInfo() {
		return this.info;
	}

	public void setProx(LSE novoProx) {
		this.prox = novoProx;
	}

	public LSE getProx() {
		return this.prox;
	}
}