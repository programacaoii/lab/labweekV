package semanaV.atvII;

public class Main {
	public static void main(String[] args) {
		String caminhoArquivo = "src/proteina1.txt";
		ManipularArquivo arquivo = new ManipularArquivo();

		MyStack<Character> conteudoLido = arquivo.lerArquivo(caminhoArquivo);

		Mapa mapa = new Mapa();
		double totalMassa;
		try {
			totalMassa = mapa.calcularTotalMassa(conteudoLido);
			System.out.println("Total massa na proteina: " + totalMassa);
			conteudoLido.pop();
		} catch (ExceptionMenssage e) {
			// conteudoLido.pop();
			System.out.println("Erro: " + e.getMessage());
			
		}

	}
}