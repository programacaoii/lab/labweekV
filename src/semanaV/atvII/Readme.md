# Calculadora de Massa de Proteína com Uso de Pilha

Um programa em Java que calcula a massa total de uma proteína a partir da sequência de aminoácidos, utilizando uma estrutura de dados do tipo pilha.

## Funcionalidades

- Calcula a massa total de uma proteína com base na sequência de aminoácidos fornecida, utilizando uma estrutura de dados do tipo pilha.

# Formato do Arquivo de Entrada
O arquivo de entrada deve conter a sequência de aminoácidos da proteína. Cada aminoácido é representado por um caractere maiúsculo, conforme especificado na descrição da atividade. O arquivo deve conter apenas caracteres válidos da lista de aminoácidos.

### Mapeamento de proteinas
<img src="../img/mapaProteina.png" width="50%"><br>

### Execução
<img src="../img/output2.png" width="70%"><br>

# Diagrama de classe
<img src="../img/diagrama2.png" width="70%"><br>
