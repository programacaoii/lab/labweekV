package semanaV.atvI;

import java.util.HashMap;
import java.util.Map;

public class Mapa {

	private final Map<Character, Double> aminoacidoMassa;

	public Mapa() {
		aminoacidoMassa = new HashMap<>();
		massasIncial();
	}

	private void massasIncial() {
		aminoacidoMassa.put('A', 71.03711);
		aminoacidoMassa.put('C', 103.00919);
		aminoacidoMassa.put('D', 115.02694);
		aminoacidoMassa.put('E', 129.04259);
		aminoacidoMassa.put('F', 147.06841);
		aminoacidoMassa.put('G', 57.02146);
		aminoacidoMassa.put('H', 137.05891);
		aminoacidoMassa.put('I', 113.08406);
		aminoacidoMassa.put('K', 128.09496);
		aminoacidoMassa.put('L', 113.08406);
		aminoacidoMassa.put('M', 131.04049);
		aminoacidoMassa.put('N', 114.04293);
		aminoacidoMassa.put('P', 97.05276);
		aminoacidoMassa.put('Q', 128.05858);
		aminoacidoMassa.put('R', 156.10111);
		aminoacidoMassa.put('S', 87.03203);
		aminoacidoMassa.put('T', 101.04768);
		aminoacidoMassa.put('V', 99.06841);
		aminoacidoMassa.put('W', 186.07931);
		aminoacidoMassa.put('Y', 163.06333);
	}

	public double calcularTotalMassa(String proteina) {
		double totalMassa = 0.0;
		for (char aminoacido : proteina.toCharArray()) {
			if (aminoacidoMassa.containsKey(aminoacido)) {
				totalMassa += aminoacidoMassa.get(aminoacido);
			} else {
				System.out.println("Aminoacido " + aminoacido + " nao esta no dicionario");
			}
			
		}
		return totalMassa;
	}
}
