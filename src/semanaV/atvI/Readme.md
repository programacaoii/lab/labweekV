# Calculadora de Massa de Proteína

Este é um programa em Java que calcula a massa total de uma proteína com base na sequência de aminoácidos fornecida. Cada aminoácido é representado por um caractere maiúsculo e tem uma massa correspondente. A massa total da proteína é calculada somando as massas individuais de cada aminoácido na sequência.

# Funcionalidades

- Calcula a massa total de uma proteína com base na sequência de aminoácidos fornecida.
- Utiliza uma estrutura de dados para mapear os aminoácidos e suas massas correspondentes.

# Formato do Arquivo de Entrada
O arquivo de entrada deve conter a sequência de aminoácidos da proteína. Cada aminoácido é representado por um caractere maiúsculo, conforme especificado na descrição da atividade. O arquivo deve conter apenas caracteres válidos da lista de aminoácidos.

### Mapeamento de proteinas
<img src="../img/mapaProteina.png" width="50%"><br>

### Execução
<img src="../img/output1.png" width="70%"><br>

# Diagrama de classe
<img src="../img/diagrama1.png" width="70%"><br>

