package semanaV.atvI;


public class Main {
	 public static void main(String[] args) {
	        String caminhoArquivo = "src/proteina1.txt";
	        ManipularArquivo arquivo = new ManipularArquivo();

	        String conteudoLido = arquivo.lerArquivo(caminhoArquivo);

	        Mapa mapa = new Mapa();
	        double totalMassa = mapa.calcularTotalMassa(conteudoLido);

	       System.out.println("Total massa na proteina: " + totalMassa);
	       
	    }
}