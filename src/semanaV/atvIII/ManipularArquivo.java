package semanaV.atvIII;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class ManipularArquivo {

	public <T> MyQueue<T> lerArquivo(String caminhoArquivo) {
		StringBuilder conteudo = new StringBuilder();
		MyQueue<Character> filaAmino = null;
		try {
			File arquivo = new File(caminhoArquivo);
			FileReader leitor = new FileReader(arquivo);
			int caractere;
			while ((caractere = leitor.read()) != -1) {
				conteudo.append((char) caractere);
			}
			filaAmino = new MyQueue<>();

			for (char aminoAcid : conteudo.toString().toCharArray()) {
				filaAmino.enqueue(aminoAcid);
			}
			leitor.close();
		} catch (IOException e) {
			System.out.println("Ocorreu um erro ao ler o arquivo: " + e.getMessage());
		}
		return (MyQueue<T>) filaAmino;
	}

}
