package semanaV.atvIII;

public class MyQueue<T> {
    private Node<T> prim;
    private Node<T> ulti;

    private static class Node<T> {
        T data;
        Node<T> prox;

        Node(T data) {
            this.data = data;
            this.prox = null;
        }
    }

    public MyQueue() {
        prim = null;
        ulti = null;
    }

    public void enqueue(T item) {
        Node<T> novoNo = new Node<>(item);
        if (ulti == null) {
            prim = novoNo;
            ulti = novoNo;
        } else {
            ulti.prox = novoNo;
            ulti = novoNo;
        }
    }

    public T dequeue() throws ExceptionMenssage {
        if (!isEmpty()) {
            T dado = prim.data;
            prim = prim.prox;
            if (prim == null) {
                ulti = null;
            }
            return dado;
        } else {
            throw new ExceptionMenssage("A fila está vazia");
        }
    }

    public boolean isEmpty() {
        return prim == null;
    }

    public int size() {
        int count = 0;
        Node<T> atual = prim;
        while (atual != null) {
            count++;
            atual = atual.prox;
        }
        return count;
    }


}
