package semanaV.atvIII;

public class Main {
	public static void main(String[] args) {
		String caminhoArquivo = "src/proteina1.txt";
		ManipularArquivo arquivo = new ManipularArquivo();

		MyQueue<Character> conteudoLido = arquivo.lerArquivo(caminhoArquivo);

		Mapa mapa = new Mapa();
		double totalMassa;
		try {
			totalMassa = mapa.calcularTotalMassa(conteudoLido);
			System.out.println("Total massa na proteina: " + totalMassa);

		} catch (ExceptionMenssage e) {
			// TODO Auto-generated catch block
			System.out.println("Erro: " + e.getMessage());
		}

	}
}